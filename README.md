**To-Do List App without Storyboard**

An app that can list down daily tasks.

This is a sample app that utilizes a full programmatic Swift approach in building the UI. This app does not use Storyboards.

---

## Features

- Display the tasks that have been listed on the app  
- Shows the priority level of each task  
- Accepts name of the task and the priority level  

---

## Demo

![](/Previews/preview.gif)
