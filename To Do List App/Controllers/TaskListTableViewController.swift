//
//  TaskListTableViewController.swift
//  To Do List App
//
//  Created by Jansen Ducusin on 2/23/21.
//

import Foundation
import UIKit

class TaskListTableViewController: UITableViewController{
    
    private var tasks = [Task]()
    private let tableViewCellIdentifier = "TaskListTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    private func setupUI(){
        self.title = "To-Do List App"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let addTaskBarButton = UIBarButtonItem.barButtonItemForAddTask(target: self, selector: #selector(addTaskBarButtonPressed))
        
        self.navigationItem.rightBarButtonItem = addTaskBarButton
    }
    
    @objc func addTaskBarButtonPressed() {
        let addTaskVC = AddTaskViewController()
        addTaskVC.delegate = self
        
        let navigationController  = UINavigationController()
        navigationController.pushViewController(addTaskVC, animated: true)
        
        self.present(navigationController, animated: true, completion: nil)
    }
}

extension TaskListTableViewController: AddTaskViewControllerDelegate{
    func addTaskViewControllerDidSave(task: Task, viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
        
        self.tasks.append(task)
        print(task)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
}

extension TaskListTableViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let task = self.tasks[indexPath.row]
        
        var cell: TaskListTableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier) as? TaskListTableViewCell
        
        if cell == nil {
            cell = TaskListTableViewCell(style: .default, reuseIdentifier: tableViewCellIdentifier)
        }
        
        cell.configure(task: task)
        
        return cell
    }
}
