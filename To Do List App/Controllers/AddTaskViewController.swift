//
//  AddTaskViewController.swift
//  To Do List App
//
//  Created by Jansen Ducusin on 2/23/21.
//

import Foundation
import UIKit

protocol AddTaskViewControllerDelegate {
    func addTaskViewControllerDidSave(task:Task, viewController: UIViewController)
}

class AddTaskViewController: UIViewController, UITextFieldDelegate {
    
    private var taskNameTextField: UITextField!
    private var prioritySegmentedControl: UISegmentedControl!
    var delegate: AddTaskViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    @objc func cancelButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveButtonPressed(){
        
        guard let title = self.taskNameTextField.text,
              let priority = Priority(rawValue: self.prioritySegmentedControl.selectedSegmentIndex) else {
            return
        }
        
        let task = Task(title: title, priority: priority)
        
        self.delegate?.addTaskViewControllerDidSave(task: task, viewController: self)
        
    }
    
    @objc func prioritySelected(segmentedControl:UISegmentedControl){
        
    }
    
    private func setupUI(){
        self.title = "Add New Task"
        self.view.backgroundColor = .white
        
        let cancelTaskbarButton = UIBarButtonItem.barButtonItemForCancel(target: self, selector: #selector(cancelButtonPressed))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = cancelTaskbarButton
        
        let saveTaskbarButton = UIBarButtonItem.barButtonItemForSave(target: self, selector: #selector(saveButtonPressed))
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = saveTaskbarButton
        
        self.taskNameTextField = self.createTaskNameTextField()
        self.prioritySegmentedControl = self.createPrioritySegmentedControl()
        let stackView = self.createStackView(for: [self.taskNameTextField, self.prioritySegmentedControl])
        
        self.view.addSubview(stackView)
        
        stackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        stackView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 20).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    private func createTaskNameTextField() -> UITextField{
        let textField = UITextField()
        textField.placeholder = "Enter task name"
        textField.borderStyle = .roundedRect
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        
        return textField
    }
    
    private func createPrioritySegmentedControl() -> UISegmentedControl{
        let segmentedControl = UISegmentedControl(items: ["High", "Medium", "Low"])
        segmentedControl.selectedSegmentIndex = 1
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.tintColor = .orange
        segmentedControl.addTarget(self, action: #selector(prioritySelected), for: .valueChanged)
        
        return segmentedControl
    }
    
    private func createStackView(for views:[UIView]) -> UIStackView{
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 5
        
        return stackView
    }

}

extension AddTaskViewController{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
