//
//  TaskListTableViewCell.swift
//  To Do List App
//
//  Created by Jansen Ducusin on 2/23/21.
//

import Foundation
import UIKit

class TaskListTableViewCell: UITableViewCell{
    
    lazy var titleLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 0.18, green: 0.20, blue: 0.21, alpha: 1.00)
        return label
        
    }()
    
    lazy var priorityView: UIButton = {
       
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 6.0
        button.layer.masksToBounds = true
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
        return button
        
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder: has not been implemented")
    }
    
    private func setupUI(){
        
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.priorityView)
        
        self.configureConstraints()
        
    }
    
    private func configureConstraints(){
        
        let marginGuide = self.contentView.layoutMarginsGuide
        
        let bottomConstraint = self.titleLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor, constant: -10)
        bottomConstraint.priority = UILayoutPriority(99)
        
        NSLayoutConstraint.activate([
            self.titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor),
            self.titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 10),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.priorityView.leadingAnchor, constant: -10),
            self.priorityView.centerYAnchor.constraint(equalTo: marginGuide.centerYAnchor),
            self.priorityView.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: -10),
            self.priorityView.widthAnchor.constraint(equalToConstant: 100)
        ])
        
    }
    
    func configure(task:Task) {
        
        let buttonTitle = task.priority
        
        self.titleLabel.text = task.title
        self.priorityView.setTitle(buttonTitle.displayTitle, for: .normal)
        
        switch buttonTitle {
        case .high:
            self.priorityView.backgroundColor = UIColor(red: 0.84, green: 0.19, blue: 0.19, alpha: 1.00)
        case .medium:
            self.priorityView.backgroundColor = UIColor(red: 0.99, green: 0.80, blue: 0.43, alpha: 1.00)
        case .low:
            self.priorityView.backgroundColor = UIColor(red: 0.00, green: 0.72, blue: 0.58, alpha: 1.00)
        }
    }
}
